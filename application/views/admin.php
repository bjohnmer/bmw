<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Zona de Administración</title>
		<?php require_once("links.php");?>
	</head>
	<body>
		<?php require_once("header.php");?>
		
		<div class="wrapper">
		<?php require_once("menu_admin.php");?>
			
			<div id="info">
				<article>
					<div id="texto">
						<h3>Zona administrativa de BMW</h3>
					</div>
				</article>
			</div>
		</div>
		<?php require_once("footer.php");?>
	</body>
</html>