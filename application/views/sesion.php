<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Sesión</title>
		<?php require_once("links.php");?>
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/sesion.css">
	</head>
	<body>
		<?php require_once("header.php");?>
		
		<div class="wrapper">
			<div id="info">
				<article>
					<div id="texto">
						<h3 class="centrado">Usuarios Autorizados</h3>
						<p>
							<form id='formulario' action="<?=base_url()?>login" method="post" accept-charset="utf-8">
								<label for="login">Usuario: </label>
								<input type="text" name="login" id="login" placeholder="Nombre de Usuario">


								<label for="password">Clave: </label>
								<input type="password" name="password" id="password" placeholder="Clave del Usuario">

								
								<?php
							      if (function_exists('validation_errors')) {
						          	echo "<div class='error'>";
							        if (validation_errors()) echo validation_errors();
							        if ($this->session->flashdata("loged_in_fail")) echo "<p>ERROR:<br>Usted a proporcionado datos inválidos<br>Intente nuevamente</p>";
						          	echo "</div>";
							      }
        						?>


								<input type="submit" value="Ingresar">

							</form>
						</p>
					</div>
				</article>
			</div>
		</div>
		<?php require_once("footer.php");?>
	</body>
</html>