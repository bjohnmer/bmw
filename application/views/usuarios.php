<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Zona de Administración</title>
		<?php require_once("links.php");?>
		<?php 
			foreach($css_files as $file): ?>
				<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
			<?php endforeach; ?>
			<?php foreach($js_files as $file): ?>
				<script src="<?php echo $file; ?>"></script>
			<?php endforeach; ?>
	</head>
	<body>
		<?php require_once("header.php");?>
		
		<div class="wrapper">
		<?php require_once("menu_admin.php");?>
			
			<div id="info">
				<article>
					<div id="texto">
						<h3>Usuarios</h3>
						<?=$output?>
					</div>
				</article>
			</div>
		</div>
		<?php require_once("footer.php");?>
	</body>
</html>