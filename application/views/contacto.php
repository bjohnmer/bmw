<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Sesión</title>
		<?php require_once("links.php");?>
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>css/sesion.css">
	</head>
	<body>
		<?php require_once("header.php");?>
		
		<div class="wrapper">
			<div id="info">
				<article>
					<div id="texto">
						<h3 class="centrado">Contáctanos</h3>
						<p>
							<form id='formulario' action="<?=base_url()?>welcome/send_mail" method="post" accept-charset="utf-8">
								<label for="nombre">Nombre: </label>
								<input type="text" name="nombre" id="nombre" placeholder="Nombres y Apellidos">


								<label for="email">Email: </label>
								<input type="text" name="email" id="email" placeholder="Correo Electrónico">

								<label for="comentario">Comentario: </label>
								<textarea name="comentario" id="comentario" cols="30" rows="10" placeholder="Comentario"></textarea>

								<?php
							      if (function_exists('validation_errors')) {
						          	echo "<div class='error'>";
							        if (validation_errors()) echo validation_errors();
						          	echo "</div>";
							      }
        						?>

								<input type="submit" value="Enviar">

							</form>
						</p>
					</div>
				</article>
			</div>
		</div>
		<?php require_once("footer.php");?>
	</body>
</html>