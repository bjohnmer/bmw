<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autos_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
	}


	// función que devuelve todos los años registrados
	public function get_years()
	{
		// SELECT DISTINCT anio_auto FROM autos

		// asignamos la sentencia sql a una variable y automáticamente se ejecuta la consulta
		$consulta = $this->db->select('anio_auto')->distinct('anio_auto')->order_by('anio_auto','desc')->get('autos');

		// preguntamos si la consulta generó resultados
		if ($consulta->num_rows()) {
			// en caso de que si, assigna esos resultados a la variable que luego será devuelta
			$data = $consulta->result();
		
		} else {
			// en caso que la consulta no devuelva ningun resultado al valor de la variable que se devolverá será falso
			$data = FALSE;
		
		}
		
		// libera la memoria del servidor
		$consulta->free_result();
		
		// devuelve los resultados
		return $data;

	}

	// función que devuelve el último año registrado
	public function get_last_year()
	{


		// asignamos la sentencia sql a una variable y automáticamente se ejecuta la consulta
		$consulta = $this->db->select('anio_auto')->distinct('anio_auto')->order_by('anio_auto','desc')->limit(1)->get('autos');

		// preguntamos si la consulta generó resultados
		if ($consulta->num_rows()) {
			// en caso de que si, assigna esos resultados a la variable que luego será devuelta
			$data = $consulta->result();
		
		} else {
			// en caso que la consulta no devuelva ningun resultado al valor de la variable que se devolverá será falso
			$data = FALSE;
		
		}
		
		// libera la memoria del servidor
		$consulta->free_result();
		
		// devuelve los resultados
		return $data;

	}


	// función que devuelve todos los registros
	public function get_all()
	{

		// asignamos la sentencia sql a una variable y automáticamente se ejecuta la consulta
		$consulta = $this->db->select()->order_by('anio_auto','desc')->get('autos');

		// preguntamos si la consulta generó resultados
		if ($consulta->num_rows()) {
			// en caso de que si, assigna esos resultados a la variable que luego será devuelta
			$data = $consulta->result();
		
		} else {
			// en caso que la consulta no devuelva ningun resultado al valor de la variable que se devolverá será falso
			$data = FALSE;
		
		}
		
		// libera la memoria del servidor
		$consulta->free_result();
		
		// devuelve los resultados
		return $data;

	}

	// función que devuelve todos los registros del último año
	public function get_all_by_year($year)
	{

		// asignamos la sentencia sql a una variable y automáticamente se ejecuta la consulta
		$consulta = $this->db->where('anio_auto',$year)->order_by('anio_auto','desc')->get('autos');

		// preguntamos si la consulta generó resultados
		if ($consulta->num_rows()) {
			// en caso de que si, assigna esos resultados a la variable que luego será devuelta
			$data = $consulta->result();
		
		} else {
			// en caso que la consulta no devuelva ningun resultado al valor de la variable que se devolverá será falso
			$data = FALSE;
		
		}
		
		// libera la memoria del servidor
		$consulta->free_result();
		
		// devuelve los resultados
		return $data;

	}

	// función que devuelve un registros
	public function get_byid($id_auto)
	{

		// asignamos la sentencia sql a una variable y automáticamente se ejecuta la consulta
		$consulta = $this->db->select()->where("id_auto", $id_auto)->order_by('anio_auto','desc')->get('autos');

		// preguntamos si la consulta generó resultados
		if ($consulta->num_rows()) {
			// en caso de que si, assigna esos resultados a la variable que luego será devuelta
			$data = $consulta->result();
		
		} else {
			// en caso que la consulta no devuelva ningun resultado al valor de la variable que se devolverá será falso
			$data = FALSE;
		
		}
		
		// libera la memoria del servidor
		$consulta->free_result();
		
		// devuelve los resultados
		return $data;

	}


	// public function getall($num,$offset)
	// {
	// 	$consulta = $this->db->join('pacientes', 'pacientes.id_paciente = historias.paciente_id')->get('historias', $num, $offset);

	// 	if ($consulta->num_rows()) {
	// 		$data = $consulta->result();
	// 	} else {
	// 		$data = FALSE;
	// 	}
	// 	$consulta->free_result();
	// 	return $data;
	// }

	// public function find($data, $num, $offset)
	// {
	// 	$sql = "SELECT historias.*, personas.*, especialidades.* FROM historias JOIN personas ON historias.persona_id = personas.id_persona JOIN especialidades ON historias.especialidad_id = especialidades.id_especialidad WHERE personas.nombres_persona LIKE ?";
		
	// 	$consulta = $this->db->query($sql, '%'.mysql_real_escape_string($data['texto']).'%');

	// 	if ($consulta->num_rows()) {
	// 		$data = $consulta->result();
	// 	} else {
	// 		$data = FALSE;
	// 	}
	// 	$consulta->free_result();
	// 	return $data;
	// }

	// public function getallnp()
	// {
	// 	$consulta = $this->db->join('pacientes', 'pacientes.id_paciente = historias.paciente_id')->get('historias');

	// 	if ($consulta->num_rows()) {
	// 		$data = $consulta->result();
	// 	} else {
	// 		$data = FALSE;
	// 	}
	// 	$consulta->free_result();
	// 	return $data;
	// }

	// public function getreg($id)
	// {
	// 	$consulta = $this->db->where("id_historia", $id)->get('historias');
	// 	if ($consulta->num_rows()) {
	// 		$data = $consulta->result();
	// 	} else {
	// 		$data = FALSE;
	// 	}
	// 	$consulta->free_result();
	// 	return $data;
	// }

	// public function getTotalCount()
	// {
	// 	$consulta = $this->db->get('historias');
	// 	if ($consulta->num_rows()) {
	// 		$data = $consulta->num_rows();
	// 	} else {
	// 		$data = FALSE;
	// 	}
	// 	$consulta->free_result();
	// 	return $data;
	// }

	// public function create($data)
	// {
	// 	/*
	// 		Captura y almacenamiento de datos en las tablas
	// 	*/
	// 	$data_persona = array (
	// 						'nombres_persona'		=> $data['nombres_persona'],
	// 						'apellidos_persona'		=> $data['apellidos_persona'],
	// 						'direccion_persona'		=> $data['direccion_persona'],
	// 						'sexo_persona'			=> $data['sexo_persona'],
	// 						'nacionalidad_persona'	=> $data['nacionalidad_persona']
	// 					);

	// 	$per = $this->db->insert('personas', $data_persona);
		
	// 	if ($per) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 	$data_paciente = array (
	// 						'persona_id'	=> $this->db->insert_id(),
	// 						'fn_paciente'	=> $data['fn_paciente'],
	// 					);
	// 	if (!empty($data['asegurado_paciente'])) $data_paciente['asegurado_paciente'] = $data['asegurado_paciente'];
	// 	if (!empty($data['beneficiario_paciente'])) $data_paciente['beneficiario_paciente'] = $data['beneficiario_paciente'];
		
	// 	$pac = $this->db->insert('pacientes', $data_paciente);

	// 	if ($pac) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


	// 	$data_historia = array (
	// 						'num_historia'	=> $data['num_historia'],
	// 						'paciente_id'	=> $this->db->insert_id(),
	// 						'tipo_historia'	=> $data['tipo_historia']
	// 					);
	// 	$his = $this->db->insert('historias', $data_historia);

	// 	if ($his) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


	// 	if (!empty($data['is_adulto'])) {
	// 		$data_adulto = array (
	// 						'paciente_id' => $data_historia['paciente_id'],
	// 						'cedula_paciente' => $data['cedula_paciente']
	// 					);
	// 		$adu = $this->db->insert('pacadulto', $data_adulto);
			
	// 		if ($adu) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 	} else {
	// 		$data_persona_rep = array (
	// 							'nombres_persona'		=> $data['nombres_persona_rep'],
	// 							'apellidos_persona'		=> $data['apellidos_persona_rep'],
	// 							'direccion_persona'		=> $data['direccion_persona_rep'],
	// 							'sexo_persona'			=> $data['sexo_persona_rep'],
	// 							'nacionalidad_persona'	=> $data['nacionalidad_persona_rep']
	// 							);
	// 		$rep = $this->db->insert('personas', $data_persona_rep);

	// 		$data_rep = array (
	// 							'persona_id'				=> $this->db->insert_id(),
	// 							'edad_representante'		=> $data['edad_representante'],
	// 							'cedula_representante'		=> $data['cedula_representante'],
	// 							'parentesco_representante'	=> $data['parentesco_representante']
	// 							);
	// 		$rep = $this->db->insert('representantes', $data_rep);
			
	// 		if ($rep) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 		$data_nino = array (
	// 							'paciente_id'		=> $data_historia['paciente_id'],
	// 							'representante_id'	=> $this->db->insert_id()
	// 						);
	// 		$nin = $this->db->insert('pacnino', $data_nino);
			
	// 		if ($nin) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 	}

	// 	if (in_array("Er",$ok)) { return FALSE; } else { return TRUE; }

	// }

	// public function update($data)
	// {
	// 	/*
	// 		Captura y modificación de datos en las tablas
	// 	*/
	// 	$data_historia = array (
	// 						'id_historia'	=> $data['id_historia'],
	// 						'num_historia'	=> $data['num_historia'],
	// 						'paciente_id'	=> $data['paciente_id'],
	// 						'tipo_historia'	=> $data['tipo_historia']
	// 					);
	// 	$his = $this->db->where('id_historia', $data['id_historia'])->update('historias', $data_historia);

	// 	if ($his) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 	$data_persona = array (
	// 						'id_persona'			=> $data['persona_id'],
	// 						'nombres_persona'		=> $data['nombres_persona'],
	// 						'apellidos_persona'		=> $data['apellidos_persona'],
	// 						'direccion_persona'		=> $data['direccion_persona'],
	// 						'sexo_persona'			=> $data['sexo_persona'],
	// 						'nacionalidad_persona'	=> $data['nacionalidad_persona']
	// 					);

	// 	$per = $this->db->where('id_persona', $data['persona_id'])->update('personas', $data_persona);
		
	// 	if ($per) { $ok[] = "Ok"; } else { $ok[] = "Er"; }


	// 	$data_paciente = array (
	// 						'id_paciente'	=> $data['paciente_id'],
	// 						'persona_id'	=> $data['persona_id'],
	// 						'fn_paciente'	=> $data['fn_paciente'],
	// 					);
	// 	if (!empty($data['asegurado_paciente'])) $data_paciente['asegurado_paciente'] = $data['asegurado_paciente']; else $data_paciente['asegurado_paciente'] = 'N';


	// 	if (!empty($data['beneficiario_paciente'])) $data_paciente['beneficiario_paciente'] = $data['beneficiario_paciente']; else $data_paciente['beneficiario_paciente'] = 'N';
		
	// 	$pac = $this->db->where('id_paciente', $data['paciente_id'])->update('pacientes', $data_paciente);

	// 	if ($pac) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 	if (!empty($data['is_adulto'])) {
	// 		$badu = $this->db->where('cedula_paciente', $data['cedula_paciente_v'])->where('paciente_id', $data['paciente_id'])->get('pacadulto');
			
	// 		$brep = $this->db->where('persona_id', $data['persona_id'])->delete('representantes');

	// 		if ($badu->num_rows()) {
	// 			$d = $badu->result();
	// 			$pac_adulto = $d[0]->id_pacadulto;
	// 			$data_adulto = array (
	// 							'id_pacadulto' => $d[0]->id_pacadulto,
	// 							'paciente_id' => $data['paciente_id'],
	// 							'cedula_paciente' => $data['cedula_paciente']
	// 						);
	// 			$adu = $this->db->where('id_pacadulto', $data_adulto['id_pacadulto'])->update('pacadulto', $data_adulto);
				
	// 			if ($adu) { $ok[] = "Ok"; } else { $ok[] = "Er"; }
	// 		}


	// 	} else {
	// 		$badu = $this->db->where('cedula_paciente', $data['cedula_paciente_v'])->where('paciente_id', $data['paciente_id'])->delete('pacadulto');

	// 		// $brep = $this->db->where('persona_id', $data['persona_id'])->get('pacadulto');
			
	// 		// if ($brep->num_rows()) {

	// 			$data_persona_rep = array (
	// 							'nombres_persona'		=> $data['nombres_persona_rep'],
	// 							'apellidos_persona'		=> $data['apellidos_persona_rep'],
	// 							'direccion_persona'		=> $data['direccion_persona_rep'],
	// 							'sexo_persona'			=> $data['sexo_persona_rep'],
	// 							'nacionalidad_persona'	=> $data['nacionalidad_persona_rep']
	// 							);
	// 			$rep = $this->db->insert('personas', $data_persona_rep);

	// 			$data_rep = array (
	// 								'persona_id'				=> $this->db->insert_id(),
	// 								'edad_representante'		=> $data['edad_representante'],
	// 								'cedula_representante'		=> $data['cedula_representante'],
	// 								'parentesco_representante'	=> $data['parentesco_representante']
	// 								);
	// 			$rep = $this->db->insert('representantes', $data_rep);
				
	// 			if ($rep) { $ok[] = "Ok"; } else { $ok[] = "Er"; }

	// 			$data_nino = array (
	// 							'paciente_id'		=> $data['paciente_id'],
	// 							'representante_id'	=> $this->db->insert_id()
	// 						);
	// 		$nin = $this->db->insert('pacnino', $data_nino);
			
	// 		if ($nin) { $ok[] = "Ok"; } else { $ok[] = "Er"; }
	// 		// }
	// 	}

	// 	if (in_array("Er",$ok)) { return FALSE; } else { return TRUE; }
	// }

	// function destroy($data)
	// {
	// 	$per = $this->db->where('id_medico', $data['id'])->delete('historias');
	// 	if ($per) {
	// 		return TRUE;
	// 	} else {
	// 		return FALSE;
	// 	}
	// }
}
