<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	// FUNCIÓN DE CONSTRUCCIÓN DE LA CLASE
	function __construct(){
		parent::__construct();
		//$this->load->helper('form');
		$this->load->library('session');
		//$this->load->library('Form_validation');
		//$this->load->library('grocery_CRUD');

		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->library('grocery_CRUD');
	}

	// FUNCIÓN DE RENDERIZADO DE LA VISTA DE VISTA
	function ver($vista = "admin", $output = null)
	{
		$this->load->view($vista,$output);	
	}

	// FUNCIÓN QUE MANDA A IMPRIMIR LA VISTA INICIAL
	public function index() {
		$this->ver();
	}
	
	// FUNCIÓN QUE MANDA A IMPRIMIR EL ÁREA DE ADMINISTRACIÓN DE AUTOS
	public function autos() {

		try{
			
			// Inicializamos el objeto de Grocery CRUD
			$crud = new grocery_CRUD();

			// Definimos la tabla en que se va a trabajar
			$crud->set_table('autos');
			$crud->set_subject('Auto');
			
			// Cambiamos la forma en que se van a mostrar los campos,
			// es decir, definimos un  Alias a cada campo
			$crud->display_as('modelo_auto','Modelo');
			$crud->display_as('anio_auto','Año');
			$crud->display_as('descripcion_auto','Descripción');
			$crud->display_as('foto_auto','Imagen');

			// Validación de los campos
			$crud->set_rules('modelo_auto', 'Modelo', 'required|alpha_space|xss_clean');
			$crud->set_rules('anio_auto', 'Año', 'required|numeric|xss_clean');
			$crud->set_rules('descripcion_auto', 'Descripción', 'required|xss_clean');
			$crud->set_rules('foto_auto', 'Imagen', 'required|xss_clean');

			// Definir el campo foto_auto como archivo(input=file) para subir una imágen
			$crud->set_field_upload('foto_auto', 'assets/uploads/files');

			// Renderizamos la salida en una variable
			$output = $crud->render();
			
			// Llamamos a la función que renderiza la vista
			$this->ver('autos', $output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	// FUNCIÓN QUE MANDA A IMPRIMIR EL ÁREA DE ADMINISTRACIÓN DE AUTOS
	public function usuarios() {

		try{
			
			// Inicializamos el objeto de Grocery CRUD
			$crud = new grocery_CRUD();

			// Definimos la tabla en que se va a trabajar
			$crud->set_table('usuarios');
			$crud->set_subject('Usuario');
			

			$crud->columns('login_usuario');


			// Cambiamos el tipo de campo a password
			$crud->change_field_type('password_usuario', 'password');
			$crud->change_field_type('cpassword_usuario', 'password');


			// Función a ejecutarse antes de Guardar y Modificar para Guardar la Clave en MD5
			$crud->callback_before_insert(array($this,'encrypt_password'));
			$crud->callback_before_update(array($this,'encrypt_password'));

			// Función a ejecutarse para que al momento de Agregar y Modificar el campo de Clave esté Vacío
			$crud->callback_edit_field('password_usuario',array($this,'set_password_input_to_empty'));
    		$crud->callback_add_field('password_usuario',array($this,'set_password_input_to_empty'));
    		$crud->callback_edit_field('cpassword_usuario',array($this,'set_cpassword_input_to_empty'));
    		$crud->callback_add_field('cpassword_usuario',array($this,'set_cpassword_input_to_empty'));



			// Cambiamos la forma en que se van a mostrar los campos,
			// es decir, definimos un  Alias a cada campo
			$crud->display_as('login_usuario','Login');
			$crud->display_as('password_usuario','Clave');
			$crud->display_as('cpassword_usuario','Confirmar Clave');

			// Validación de los campos
			$crud->set_rules('login_usuario', 'Login del Usuario', 'required|alpha');
			$crud->set_rules('password_usuario', 'Clave del Usuario', 'required|alpha_dash|matches[cpassword_usuario]');
			$crud->set_rules('cpassword_usuario', 'Campo de Confirmación', 'required|alpha_dash|matches[password_usuario]');

			// Renderizamos la salida en una variable
			$output = $crud->render();
			
			// Llamamos a la función que renderiza la vista
			$this->ver('usuarios', $output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	// Función que Encripta la Clave antes de Guardarla
	function encrypt_password($post_array, $primary_key = null) {
    	$this->load->helper('security');
    	$post_array['password_usuario'] = do_hash($post_array['password_usuario'], 'md5');
    	$post_array['cpassword_usuario'] = do_hash($post_array['cpassword_usuario'], 'md5');
    	// Devuelve el arreglo para Guardar
    	return $post_array;
    }

    // Función a ejecutarse para que al momento de Agregar y Modificar el campo de Clave esté Vacío
    function set_password_input_to_empty() {
	    return "<input type='password' name='password_usuario' value='' />";
	}

	// Función a ejecutarse para que al momento de Agregar y Modificar el campo de Confirmar Clave esté Vacío
    function set_cpassword_input_to_empty() {
	    return "<input type='password' name='cpassword_usuario' value='' />";
	}

}