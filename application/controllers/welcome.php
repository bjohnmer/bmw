<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{

		parent::__construct();

		$this->load->model("autos_model","autos");
		
	}

	public function index()
	{

		$last_year = $this->autos->get_last_year();

		$data['anios'] = $this->autos->get_years();

		$data['autos'] = $this->autos->get_all_by_year($last_year[0]->anio_auto);

		$this->load->view('welcome_message', $data);
		
	}

	public function filtro()
	{

		$year = $_POST['anio'];

		$data['anios'] = $this->autos->get_years();

		$data['autos'] = $this->autos->get_all_by_year($year);

		$this->load->view('welcome_message', $data);

	}

	public function details()
	{

		$data['anios'] = $this->autos->get_years();

		$data['auto'] = $this->autos->get_byid($this->uri->segment(3));

		$this->load->view('details', $data);
		
	}

	public function res() 
	{

		$this->load->view('resena');

	}

	public function mis() 
	{

		$this->load->view('mision');

	}

	public function vis() 
	{

		$this->load->view('vision');

	}

	public function ses() 
	{

		$this->load->library('session');

		if ($this->session->userdata("logged_in")){
			
			$this->load->view('admin');			
		}
		else
		{
			$this->load->view('sesion');
		}

	}

	public function con() 
	{

		$this->load->view('contacto');

	}

	public function send_mail() 
	{


		$this->load->helper('email');
		$this->load->library('Form_validation');
		
		$this->form_validation->set_rules("nombre", "Nombres y Apellidos", "required|alpha_space|xss_clean");
		$this->form_validation->set_rules("email", "Correo Electrónico", "required|trim|valid_email|xss_clean");
		$this->form_validation->set_rules("comentario", "Comentario", "required|xss_clean");

		if ($this->form_validation->run() == TRUE) {
			
			$nombre = $_POST['nombre'];
			$email = $_POST['email'];
			$comentario = $_POST['comentario'];

			$mensaje = $email.'<br>'.$nombre.'<br><br>'.$comentario;

			send_email('bjohnmer@gmail.com', 'Contacto', $mensaje);
		} 
		$this->load->view('contacto');

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */