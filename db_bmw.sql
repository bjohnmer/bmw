-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 02, 2012 at 07:47 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_bmw`
--

-- --------------------------------------------------------

--
-- Table structure for table `autos`
--

CREATE TABLE IF NOT EXISTS `autos` (
  `id_auto` int(11) NOT NULL AUTO_INCREMENT,
  `modelo_auto` varchar(50) NOT NULL,
  `anio_auto` int(11) NOT NULL,
  `descripcion_auto` text NOT NULL,
  `foto_auto` varchar(150) NOT NULL,
  PRIMARY KEY (`id_auto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `autos`
--

INSERT INTO `autos` (`id_auto`, `modelo_auto`, `anio_auto`, `descripcion_auto`, `foto_auto`) VALUES
(1, 'BMW Serie 3 Sedan', 2010, 'El nuevo BMW Serie 3 se presenta, por primera vez, con las Líneas Sport, Luxury y Modern. Decídase por uno de estos modelos de carácter expresivo y adapte el vehículo a sus deseos. El nuevo BMW Serie 3 sigue siendo un vehículo de referencia en su sexta generación.o.', 'uploads/sedan2012.jpg'),
(2, 'BMW Serie 6 Coupé', 2011, 'Inspirado en el flujo del movimiento, con formas claras y un dinamismo sin concesiones: el BMW Serie 6 impresiona por sus potentes motores y seduce con su exclusiva elegancia.', 'uploads/coupe2012.jpg'),
(3, 'BMW Serie 7 ActiveHybrid', 2012, 'El siguiente capítulo de BMW EfficientDynamics. Experimente el BMW Serie 7 ActiveHybrid: la forma más elegante de disfrutar de una eficacia que marca tendencia, de un placer de conducir en estado puro y de una comodidad excepcional', 'uploads/activehybrid.jpg'),
(4, 'BMW Serie 3 Cabrio', 2010, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur accumsan sagittis lorem, in pulvinar libero egestas quis. Suspendisse potenti. Curabitur a dui ac ligula dictum rutrum feugiat eget sapien. Nulla ornare convallis augue ac mollis. Quisque ut sem orci. Sed consequat odio ac nunc euismod pulvinar. Aliquam vel massa non nibh facilisis elementum. Nam eleifend feugiat arcu vel dignissim. Curabitur malesuada nisi a arcu tempor luctus eleifend tellus aliquam. Nullam vulputate arcu sed dui consectetur sit amet eleifend sapien blandit. Phasellus lobortis nisi eget ante pharetra sodales. Mauris quis nunc felis, id varius massa.', 'uploads/cabrio2010.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `login_usuario` varchar(20) NOT NULL,
  `password_usuario` varchar(50) NOT NULL,
  `cpassword_usuario` varchar(50) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
